using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models.DTO;
using AutoMapper;
using tech_test_payment_api.Models.Entity;

namespace tech_test_payment_api.Models.Mapper
{
    public class VendaMappingProfile : Profile
    {
          public VendaMappingProfile()
        {
            CreateMap< CadastrarVendaDTO, Venda>().ReverseMap();
            CreateMap<CadastrarProdutoDTO, Produto>().ReverseMap();
            CreateMap<CadastrarVendedorDTO, Vendedor>().ReverseMap();
            CreateMap<Venda, VendaDTO>().ReverseMap();
            CreateMap<Produto, ProdutoDTO>().ReverseMap();
            CreateMap<Vendedor, VendedorDTO>().ReverseMap();
        }
    }
}