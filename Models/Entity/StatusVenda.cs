using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;


namespace tech_test_payment_api.Models.Entity
{
    public enum StatusVenda
    {
         [Description("Aguardando Pagamento")]
        AguardandoPagamento,
        [Description("Pagamento Aprovado")]
        PagamentoAprovado,
        [Description("Cancelada")]
        Cancelada,
        [Description("Enviado para transportadora")]
        EnviadoParaTransportadora,
        [Description("Entregue")]
        Entregue
    }
}